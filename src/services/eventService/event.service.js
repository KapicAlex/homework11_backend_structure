const db = require("../../data/db/connections/connection");
const stats = require("../../config/stats");
const convertEventInfo = require("../../helpers/convertToFrontend/convertEventInfo");
const convertOddstInfo = require("../../helpers/convertToFrontend/convertOddsInfo");

const createEvent = (req, res) => {
  db("odds")
      .insert(req.body.odds)
      .returning("*")
      .then(([odds]) => {
        delete req.body.odds;
        req.body.away_team = req.body.awayTeam;
        req.body.home_team = req.body.homeTeam;
        req.body.start_at = req.body.startAt;
        delete req.body.awayTeam;
        delete req.body.homeTeam;
        delete req.body.startAt;
        db("event")
          .insert({
            ...req.body,
            odds_id: odds.id,
          })
          .returning("*")
          .then(([event]) => {
            stats.totalEvents++;
            convertEventInfo(event);
            convertOddstInfo(odds)
            return res.send({
              ...event,
              odds,
            });
          });
      });
}


const updateEvent = (req, res) => {
  db("bet")
      .where("event_id", req.params.id)
      .andWhere("win", null)
      .then((bets) => {
        var [w1, w2] = req.body.score.split(":");
        let result;
        if (w1 > w2) {
          result = "w1";
        } else if (w2 > w1) {
          result = "w2";
        } else {
          result = "x";
        }

        db("event")
          .where("id", req.params.id)
          .update({ score: req.body.score })
          .returning("*")
          .then(([event]) => {
            Promise.all(
              bets.map((bet) => {
                if (bet.prediction === result) {
                  db("bet").where("id", bet.id).update({
                    win: true,
                  });
                  db("user")
                    .where("id", bet.user_id)
                    .then(([user]) => {
                      return db("user")
                        .where("id", bet.user_id)
                        .update({
                          balance:
                            user.balance + bet.bet_amount * bet.multiplier,
                        });
                    });
                } else if (bet.prediction !== result) {
                  return db("bet").where("id", bet.id).update({
                    win: false,
                  });
                }
              })
            );
            convertEventInfo(event)
              res.send(event);
          });
      });
}
module.exports = { createEvent, updateEvent };