const db = require("../../data/db/connections/connection");
const stats = require("../../config/stats");
const convertBetsInfo = require("../../helpers/convertToFrontend/convertBetsInfo");
const errorHandler = require("../../middlewares/error/errorHandler");

const createBet = (req, res, userId) => {
  db.select().table("user").then((users) => {
    const user = users.find(user => user.id === userId);
    if(!user) {
      errorHandler(req, res, "User does not exist");
      return;
    }
    if(user.balance < req.body.bet_amount) {
      return errorHandler(req, res, "Not enough balance");
    }

    db("event").where("id", req.body.event_id).then(([event]) => {
      if(!event) {
        return errorHandler(req, res, "Event not found");
      }

      db("odds").where("id", event.odds_id).then(([odds]) => {
        if(!odds) {
          return errorHandler(req, res, "Odds not found");
        }

        let multiplier;
        switch(req.body.prediction) {
          case "w1":
            multiplier = odds.home_win;
          break;
          case "w2":
            multiplier = odds.away_win;
          break;
          case "x":
            multiplier = odds.draw;
          break;
        }
        
        insertBet(event, user, req, res, multiplier, userId);
      })
    })
  })
};

const insertBet = (event, user, req, res, multiplier, userId) => {
  db("bet")
    .insert({
      ...req.body,
      multiplier,
      event_id: event.id
    })
    .returning("*")
    .then(([bet]) => {
      const currentBalance = user.balance - req.body.bet_amount;
      db("user").where("id", userId).update({
        balance: currentBalance,
      }).then(() => {
        stats.totalBets++;
        convertBetsInfo(bet)
        return res.send({ 
          ...bet,
          currentBalance: currentBalance,
        });
      });
    })
}

module.exports = createBet;