const db = require("../../data/db/connections/connection");

const getUserById = async (id) => {
  return await db("user").where("id", id).returning("*")
}

const createUser = async (reqBody) => {
  return await db("user").insert(reqBody).returning("*")
}

const updateUser = async (id, reqBody) => {
  return await db("user")
                .where("id", id)
                .update(reqBody)
                .returning("*")
}

module.exports = { getUserById, createUser, updateUser }