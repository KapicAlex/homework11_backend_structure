const db = require("../../data/db/connections/connection");
const convertTransactionInfo = require("../../helpers/convertToFrontend/convertTransactionInfo.js");

const createTransaction = (req, res) => {
  db("user")
    .where("id", req.body.userId)
    .then(([user]) => {
      if (!user) {
        errorHandler(req, res, "User does not exist");
        return;
      }
      req.body.card_number = req.body.cardNumber;
      delete req.body.cardNumber;
      req.body.user_id = req.body.userId;
      delete req.body.userId;

      db("transaction")
        .insert(req.body)
        .returning("*")
        .then(([result]) => {
          var currentBalance = req.body.amount + user.balance;
          db("user")
            .where("id", req.body.user_id)
            .update("balance", currentBalance)
            .then(() => {
              convertTransactionInfo(result);
              return res.send({
                ...result,
                currentBalance,
              });
            });
        });
    });
}

module.exports = createTransaction;