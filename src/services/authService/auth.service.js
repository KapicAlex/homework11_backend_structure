const jwt = require("jsonwebtoken");
const stats = require("../../config/stats")
const errorHandler = require("../../middlewares/error/errorHandler");

const authBets = (req, res) => {
  let token = req.headers["authorization"];
    if(!token) {
      return errorHandler(req, res, "notAuth");
    }
    token = token.replace("Bearer ", "");
    try {
      const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      return tokenPayload.id;
    } catch (err) {
      console.log(err);
      return errorHandler(req, res, "notAuth");
    }
}

const authEvents = (req, res) => {
  let token = req.headers["authorization"];
  if (!token) {
    return errorHandler(req, res, "notAuth");
  }
  token = token.replace("Bearer ", "");
  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    stats.totalEvents--
    if (tokenPayload.type !== "admin") {
      throw new Error();
    }
    stats.totalEvents++
  } catch (err) {
    console.log(err);
    return errorHandler(req, res, "notAuth");
  }
}

const authStats = (req, res) => {
  let token = req.headers["authorization"];
    if (!token) {
      return errorHandler(req, res, "notAuth");
    }
    token = token.replace("Bearer ", "");
    try {
      const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
      if (tokenPayload.type != "admin") {
        throw new Error();
      }
    } catch (err) {
      return errorHandler(req, res, "notAuth");
    }
    res.send(stats);
}

const authTransitions = (req, res) => {
  let token = req.headers["authorization"];
  if (!token) {
    return errorHandler(req, res, "notAuth");
  }
  token = token.replace("Bearer ", "");
  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    if (tokenPayload.type !== "admin") {
      throw new Error();
    }
  } catch (err) {
    return errorHandler(req, res, "notAuth");
  }
}

const authUsers = (req, res) => {
  let token = req.headers["authorization"];
  if (!token) {
    return errorHandler(req, res, "notAuth");
  }
  token = token.replace("Bearer ", "");
  try {
    return jwt.verify(token, process.env.JWT_SECRET);
  } catch (err) {
    return errorHandler(req, res, "notAuth");
  }
}
module.exports = { authBets, authEvents, authStats, authTransitions, authUsers };