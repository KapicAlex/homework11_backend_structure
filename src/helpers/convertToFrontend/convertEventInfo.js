const convertEventInfo = (event) => {
  const frontData = [
    "betAmount",
    "eventId",
    "awayTeam",
    "homeTeam",
    "oddsId",
    "startAt",
    "updatedAt",
    "createdAt",
  ];
  const backData = [
    "bet_amount",
    "event_id",
    "away_team",
    "home_team",
    "odds_id",
    "start_at",
    "updated_at",
    "created_at",
  ]

  frontData.forEach((key, idx) => {
    event[key] = event[backData[idx]];
    delete event[backData[idx]];
  })
}
module.exports = convertEventInfo;