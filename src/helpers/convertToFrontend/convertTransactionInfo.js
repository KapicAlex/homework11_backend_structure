const convertTransactionInfo = (transaction) => {
  const frontData = [
    "userId",
    "cardNumber",
    "createdAt",
    "updatedAt"
  ];
  const backData = [
    "user_id",
    "card_number",
    "created_at",
    "updated_at"
  ]

  frontData.forEach((key, idx) => {
    transaction[key] = transaction[backData[idx]];
    delete transaction[backData[idx]];
  })
}
module.exports = convertTransactionInfo;