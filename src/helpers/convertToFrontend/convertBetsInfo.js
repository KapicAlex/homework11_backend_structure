const convertBetsInfo = (bet) => {
  const frontData = [
    "betAmount",
    "eventId",
    "awayTeam",
    "homeTeam",
    "oddsId",
    "startAt",
    "updatedAt",
    "createdAt",
    "userId"
  ];
  const backData = [
    "bet_amount",
    "event_id",
    "away_team",
    "home_team",
    "odds_id",
    "start_at",
    "updated_at",
    "created_at",
    "user_id"
  ]

  frontData.forEach((key, idx) => {
    bet[key] = bet[backData[idx]];
    delete bet[backData[idx]];
  })
}
module.exports = convertBetsInfo;