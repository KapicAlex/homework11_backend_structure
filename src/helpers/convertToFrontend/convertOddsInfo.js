const convertOddsInfo = (odds) => {
  const frontData = [
    "homeWin",
    "awayWin",
    "createdAt",
    "updatedAt"
  ];
  const backData = [
    "home_win",
    "away_win",
    "created_at",
    "updated_at"
  ]

  frontData.forEach((key, idx) => {
    odds[key] = odds[backData[idx]];
    delete odds[backData[idx]];
  })
}
module.exports = convertOddsInfo;