const joi = require("joi");

const postTransactions = (reqBody) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      userId: joi.string().uuid().required(),
      cardNumber: joi.string().required(),
      amount: joi.number().min(0).required(),
    })
    .required();

  return schema.validate(reqBody)
}

module.exports = { postTransactions }