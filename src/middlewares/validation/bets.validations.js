const joi = require("joi");

const postBets = (reqBody) => {
  const schema = joi.object({
    id: joi.string().uuid(),
    eventId: joi.string().uuid().required(),
    betAmount: joi.number().min(1).required(),
    prediction: joi.string().valid("w1", "w2", "x").required(),
  }).required();

  return schema.validate(reqBody);
}

module.exports = { postBets }