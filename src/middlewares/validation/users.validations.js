const joi = require("joi");

const getUsers = (reqParams) => {
  const schema = joi
      .object({
        id: joi.string().uuid(),
      })
      .required();

  return schema.validate(reqParams);
}

const postUsers = (reqBody) => {
  const schema = joi
    .object({
      id: joi.string().uuid(),
      type: joi.string().required(),
      email: joi.string().email().required(),
      phone: joi
        .string()
        .pattern(/^\+?3?8?(0\d{9})$/)
        .required(),
      name: joi.string().required(),
      city: joi.string(),
    })
    .required();
  
  return schema.validate(reqBody);
}

const putUsers = (reqBody) => {
  const schema = joi
    .object({
      email: joi.string().email(),
      phone: joi.string().pattern(/^\+?3?8?(0\d{9})$/),
      name: joi.string(),
      city: joi.string(),
    })
    .required();

  return schema.validate(reqBody);
}

module.exports = {
  getUsers,
  postUsers,
  putUsers
}