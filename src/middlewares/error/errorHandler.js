const { postBets } = require("../../middlewares/validation/bets.validations");
const { postEvents, putEvents } = require("../../middlewares/validation/events.validations");
const { postTransactions } = require("../../middlewares/validation/transactions.validations");
const { getUsers, postUsers, putUsers } = require("../../middlewares/validation/users.validations");


const errorHandler = (req, res, errorMessage) => {
  switch (errorMessage) {
    case "postBetsError":
      return res.status(400).send({ error: postBets(req.body).error.details[0].message });
    case "serverError":
      return res.status(500).send("Internal Server Error");
    case "postEventError":
      return res.status(400).send({ error: postEvents(req.body).error.details[0].message });
    case "putEventError":
      return res.status(400).send({ error: putEvents(req.body).error.details[0].message });
    case "postTransactionError":
      return res.status(400).send({ error: postTransactions(req.body).error.details[0].message });
    case "getUserError":
      return res.status(400).send({ error: getUsers(req.params).error.details[0].message });
    case "getUserIdError":
      return res.status(404).send({ error: "User not found" });
    case "postUserError":
      return res.status(400).send({ error: postUsers(req.body).error.details[0].message });
    case "23505":
      return res.status(400).send({ error: err.detail });
    case "putUserError":
      return res.status(400).send({ error: putUsers(req.body).error.details[0].message });
    case "putUserIdError":
      return res.status(401).send({ error: "UserId mismatch" });

    case "notAuth":
      return res.status(401).send({ error: "Not Authorized" }); 

    case "User does not exist":
      res.status(400).send({ error: "User does not exist"});
    case "Not enough balance":
      return res.status(400).send({ error: "Not enough balance" });
    case "Event not found":
      return res.status(404).send({ error: "Event not found" });
    case "Odds not found":
      return res.status(404).send({ error: "Odds not found" });
    default:
      break;
  }
}
module.exports = errorHandler;