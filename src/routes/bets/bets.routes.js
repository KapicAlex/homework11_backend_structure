const Router = require("express");
const router = Router();

const { postBets } = require("../../middlewares/validation/bets.validations");
const createBet = require("../../services/betsService/bets.service");
const { authBets } = require("../../services/authService/auth.service");
const  errorHandler  = require("../../middlewares/error/errorHandler");

router.post("/", (req, res) => {
  if(postBets(req.body).error) {
    errorHandler(req, res, "postBetsError")
    return;
  };
  
  try {
    const userId = authBets(req, res);

    req.body.event_id = req.body.eventId;
    req.body.bet_amount = req.body.betAmount;
    delete req.body.eventId;
    delete req.body.betAmount;
    req.body.user_id = userId;

    createBet(req, res, userId);
  } catch (err) {
    console.log(err);
    errorHandler(req, res, "serverError");
    return;
  }
});

module.exports = router;