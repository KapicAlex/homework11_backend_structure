const betsRoutes = require("./bets/bets.routes");
const eventsRoutes = require("./events/events.routes");
const statsRoutes = require("./stats/stats.routes");
const transactionsRoutes = require("./transactions/transactions.routes");
const usersRoutes = require("./users/users.routes");
const healthRoutes = require("./health/health.routes")

module.exports = (app) => {
  app.use("/bets", betsRoutes);
  app.use("/events", eventsRoutes);
  app.use("/stats", statsRoutes);
  app.use("/transactions", transactionsRoutes);
  app.use("/users", usersRoutes);
  app.use("/health", healthRoutes);
}