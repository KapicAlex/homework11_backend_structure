const Router = require("express");
const router = Router();

const { authStats } = require("../../services/authService/auth.service");
const errorHandler = require("../../middlewares/error/errorHandler");


router.get("/", (req, res) => {
  try {
    authStats(req, res);
  } catch (err) {
    console.log(err);
    errorHandler(req, res, "serverError");
    return;
  }
});

module.exports = router;
