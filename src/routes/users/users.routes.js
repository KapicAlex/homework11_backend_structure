const Router = require("express");
const router = Router();
const jwt = require("jsonwebtoken");

const stats = require("../../config/stats")
const { getUsers, postUsers, putUsers } = require("../../middlewares/validation/users.validations");
const { getUserById, createUser, updateUser } = require("../../services/userService/user.service")
const { authUsers } = require("../../services/authService/auth.service");
const errorHandler = require("../../middlewares/error/errorHandler");

router.get("/:id", (req, res) => {
  try {
    if (getUsers(req.params).error) {
      errorHandler(req, res, "getUserError");
      return;
    }

    getUserById(req.params.id)
      .then(([result]) => {
        if (!result) {
          errorHandler(req, res, "getUserIdError")
          return;
        }
        return res.send({
          ...result,
        });
      });

  } catch (err) {
    console.log(err);
    errorHandler(req, res, "serverError");
    return;
  }
});

router.post("/", (req, res) => {
  if (postUsers(req.body).error) {
    errorHandler(req, res, "postUserError");
    return;
  }
  req.body.balance = 0;

  createUser(req.body)
    .then(([result]) => {
      result.createdAt = result.created_at;
      delete result.created_at;
      result.updatedAt = result.updated_at;
      delete result.updated_at;

      stats.totalUsers++;
      return res.send({
        ...result,
        accessToken: jwt.sign(
          { id: result.id, type: result.type },
          process.env.JWT_SECRET
        ),
      });
    })
    .catch((err) => {
      if (err.code === "23505") {
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      errorHandler(req, res, "serverError");
      return;
    });
});

router.put("/:id", (req, res) => {
  const tokenPayload = authUsers(req, res);

  if (putUsers(req.body).error) {
    errorHandler(req, res, "putUserError");
    return;
  }
  if (req.params.id !== tokenPayload.id) {
    errorHandler(req, res, "putUserIdError");
  }

  updateUser(req.params.id, req.body)
    .then(([result]) => {
      return res.send({
        ...result,
      });
    })
    .catch((err) => {
      if (err.code === "23505") {
        console.log(err);
        res.status(400).send({
          error: err.detail,
        });
        return;
      }
      console.log(err);
      errorHandler(req, res, "serverError");
      return;
    });
});

module.exports = router;
