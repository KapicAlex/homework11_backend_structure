const Router = require("express");
const router = Router();

const { postEvents, putEvents } = require("../../middlewares/validation/events.validations")
const { createEvent, updateEvent } = require("../../services/eventService/event.service")
const { authEvents } = require("../../services/authService/auth.service");
const errorHandler = require("../../middlewares/error/errorHandler");

router.post("/", (req, res) => {
  if (postEvents(req.body).error) {
    errorHandler(req, res, "postEventError")
    return;
  }

  try {
    authEvents(req, res);

    req.body.odds.home_win = req.body.odds.homeWin;
    delete req.body.odds.homeWin;
    req.body.odds.away_win = req.body.odds.awayWin;
    delete req.body.odds.awayWin;

    createEvent(req, res);
  } catch (err) {
    console.log(err);
    errorHandler(req, res, "serverError");
    return;
  }
});

router.put("/:id", (req, res) => {
  if (putEvents(req.body).error) {
    errorHandler(req, res, "putEventError")
    return;
  }

  try {
    authEvents(req, res);
    updateEvent(req, res)
  } catch (err) {
    console.log(err);
    errorHandler(req, res, "serverError");
    return;
  }
});

module.exports = router;
