const Router = require("express");
const router = Router();

const { postTransactions } = require("../../middlewares/validation/transactions.validations");
const createTransaction = require("../../services/transactionService/trasaction.service")
const { authTransitions } = require("../../services/authService/auth.service");
const errorHandler = require("../../middlewares/error/errorHandler");

router.post("/", (req, res) => {
  if (postTransactions(req.body).error) {
    errorHandler(req, res, "postTransactionError");
    return;
  }

  authTransitions(req, res)

  try {
    createTransaction(req, res)
  } catch (err) {
    errorHandler(req, res, "serverError");
      return;
    };
});

module.exports = router;
